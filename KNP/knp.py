from numpy import random
import sys
import numpy as np
import matplotlib.pyplot as plt
import networkx as nw

n, k = 15, 3

def lync_all(weight, tree, connectPoint):
    minI = sys.maxsize
    i_min, j_min = None, None
    for i in range(n):
        if connectPoint[i] == 1:
            for j in range(n):
                if connectPoint[j] == 0:
                    if minI > weight[i][j]:
                        minI = weight[i][j]
                        i_min, j_min = i, j
    tree[i_min][j_min] = minI
    tree[j_min][i_min] = minI
    weight[i_min][j_min] = weight[j_min][i_min] = sys.maxsize
    connectPoint[i_min] = connectPoint[j_min] = 1

    def removeConnection(tree):
    maxI = 0
    i_max = j_max = 0
    for i in range(n):
        for j in range(i + 1, n):
            if tree[i][j] > maxI:
                maxI = tree[i][j]
                i_max, j_max = i, j
    tree[i_max][j_max] = tree[j_max][i_max] = 0

def connectionStart(weight, tree):
    connectPoint = [0 for i in range(n)]
    minI = weight[0][1]
    i_min, j_min = 0, 1
    for i in range(n):
        for j in range(i + 1, n):
            if minI > weight[i][j]:
                minI = weight[i][j]
                i_min, j_min = i, j
    tree[i_min][j_min] = minI
    tree[j_min][i_min] = minI
    weight[i_min][j_min] = weight[j_min][i_min] = sys.maxsize
    connectPoint[i_min] = connectPoint[j_min] = 1
    return connectPoint


weight = [[0 for i in range(n)] for i in range(n)]
for i in range(0, n):
    for j in range(i + 1, n):
        weight[i][j] = np.random.randint(1, 100)
        weight[j][i] = weight[i][j]

#check weights
print("Weights:")
for i in range(0, n):
    print(weight[i])

tree = [[0 for i in range(n)] for j in range(n)]
for i in range(0, n):
    print(tree[i])
connectPoint = connectionStart(weight, tree)
while 0 in connectPoint:
    lync_all(weight, tree, connectPoint)
for i in range(k - 1):
    removeConnection(tree)


#drawing
graph = nw.Graph(strict=False)
for i in range(0, n):
    graph.add_node(i)
for i in range(0, n):
    for j in range(0, n):
        if tree[i][j] == 0:
            continue
        graph.add_edge(i, j, weight=tree[i][j])
        tree[i][j] = tree[j][i] = 0
nw.draw_circular(graph, with_labels=True)
pos = nw.circular_layout(graph)
edge_labels = nw.get_edge_attributes(graph, 'weight')
nw.draw_networkx_edge_labels(graph, pos=pos, edge_labels=edge_labels)
plt.show()
