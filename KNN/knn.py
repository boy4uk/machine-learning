import matplotlib.pyplot as pplt
import numpy
import random as rnd

def getNoClassData(class_Num, clasES_num):
    data = []
    for class_Num in range(clasES_num):
        centerX, centerY = rnd.rnd() * 5.0, rnd.rnd() * 5.0
        for rowNum in range(int(class_Num)):
            data.append([rnd.gauss(centerX, 0.5), rnd.gauss(centerY, 0.5)])
    return data

def getClassData(class_Num, clasES_num):
    data = []
    for class_Num in range(clasES_num):
        centerX, centerY = rnd.rnd() * 5.0, rnd.rnd() * 5.0
        for rowNum in range(int(class_Num)):
            data.append([[rnd.gauss(centerX, 0.5), rnd.gauss(centerY, 0.5)], class_Num])
    return data

def dist(x1, y1, x2, y2):
    return numpy.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

colors = ['darkmorange', 'b', 'g', 'r', 'y', 'k', 'w', 'm']

def plot(x, y, color_index):
    pplt.scatter(x, y, c=colors[color_index])


k = 20
clusters = []
cl = 4
objectsCount = cl * 400
classObjectsCount = objectsCount * 0.8
unClassedObjectsCount = objectsCount * 0.2
classedObjects = getClassData(classObjectsCount / cl, cl)
needsToBeClassed = getNoClassData(unClassedObjectsCount / cl, cl)

for i in range(len(classedObjects)):
    color = classedObjects[i][1] + 1
    plot(classedObjects[i][0][0], classedObjects[i][0][1], color)

pplt.show()

for i in range(len(needsToBeClassed)):
    plot(needsToBeClassed[i][0], needsToBeClassed[i][1], 0)

pplt.show()

for i in range(len(needsToBeClassed)):
    neighbours = []
    for j in range(len(classedObjects)):
        distance_to_point = dist(needsToBeClassed[i][0],
                                 needsToBeClassed[i][1],
                                 classedObjects[j][0][0],
                                 classedObjects[j][0][1])
        point_cluster_number = classedObjects[j][1]
        neighbours.append([distance_to_point, point_cluster_number])
    neighbours.sort()
    nearestCluster = [neighbours[i] for i in range(k)]

    max_neighbours = 0
    maxNeighbourCluster = 0

    clusters_c = numpy.zeros(cl)
    for j in range(len(nearestCluster)):
        clusters_c[nearestCluster[j][1]] += 1

    j = 0
    for neighbours_in_cluster in clusters_c:
        if neighbours_in_cluster > max_neighbours:
            max_neighbours = neighbours_in_cluster
            maxNeighbourCluster = j
        j = j + 1
    clusters.append(maxNeighbourCluster)

for i in range(len(needsToBeClassed)):
    color = clusters[i] + 1
    plot(needsToBeClassed[i][0], needsToBeClassed[i][1], color)

pplt.show()