import pandas
import matplotlib.pyplot
import seaborn


def dataReader(file):
    data = pandas.read_csv(file, delimiter=",")
    return data[["Gender", "Age"]]


dataBefore = dataReader("train.csv")
dataAfter = dataBefore.groupby(['Gender'], as_index=False).mean()
seaborn.barplot(x='Gender', y='Age', data=dataAfter)

matplotlib.pyplot.show()
