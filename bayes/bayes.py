import numpy as np
import pandas as pd

disease = pd.read_csv(r'disease.csv', sep=';')
symptom = pd.read_csv(r'symptom.csv', sep=';')

patientsCountTable = disease['количество пациентов'].to_numpy()
totalPatientsCount = patientsCountTable[len(patientsCountTable) - 1]

testData = [np.random.randint(2) for i in range(len(symptom) - 1)]
print(testData)

illnessCount = illnesses.head(len(illnesses) - 1)
illnessPercent = [1 for i in range(0, len(illnessCount))]
illnesses = disease['Болезнь']
proportionsToTotalCount = [patientsCountTable[i] / totalPatientsCount for i in range(len(patientsCountTable) - 1)]
for i in range(len(illnessCount)):
    illnessPercent[i] *= proportionsToTotalCount[i]
    for j in range(len(symptom) - 1):
        if testData[j] == 1:
            illnessPercent[i] *= float(symptom.iloc[j][i + 1].replace(',', '.'))

mostPossibleDiseaseIndex = illnessPercent.index(max(illnessPercent))
verdict = disease['Болезнь'][mostPossibleDiseaseIndex]
print(verdict)